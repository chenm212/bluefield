#!/usr/bin/python
import socket, time
import re

ListenPort = 21 
IPAddress="10.0.0.10"
timetosleep=1

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((IPAddress, ListenPort))
serversocket.listen(5)

while 1:
 print "Listening on port", ListenPort
 conn, addr = serversocket.accept()
 print "Connection from ", addr
 data = ""
 try:
  conn.send("220-FTP Server version 2.2 \r\nWelcome to ProFtpd Server\r\n") 
  conn.send("220 Authorised Use Only\r\n") 
 except:
  conn.close()
  
 while 1:
  try:  
   myinput = conn.recv(1000)
   data = data + myinput
  except:
   conn.close()
   break
  print myinput
  if myinput.count("USER"):
   time.sleep(timetosleep)
   conn.send("331 Password required.\r\n")
  if myinput.count("PASS"):
   time.sleep(timetosleep)
   conn.send("230 Authenticated.\r\n")
  if myinput.count("PWD") or myinput.count("SYST"):
   time.sleep(timetosleep)   
   conn.send("257 \"/\" is your current location\r\n")
  if myinput.count("XPWD"):
   time.sleep(timetosleep)   
   conn.send("250 OK. Current directory is /\r\n")
  if myinput.count("RETR"):
   conn.send("360 - Cannot get file\r\n")
  if myinput.count("LIST") or myinput.count("NLST") or  myinput.count("PORT"):
   time.sleep(5)
   conn.send("drwxr-xr-x  2 root root     4096 2011-05-07 20:46 Desktop\r\ndrwxr-xr-x  7 root root     4096 2012-06-09 04:59 vmware-tools-distrib\r\n")
  if myinput.count("PASV"):
   time.sleep(timetosleep)   
   conn.send("227 Entering Passive Mode\r\n")
  if myinput == "":
   break
 conn.close()
 flFile = file("/root/ftp.log", "a")
 flFile.write(addr[0])
 flFile.write("\n")
 flFile.write(data)
 flFile.write("\n")
 flFile.close()
