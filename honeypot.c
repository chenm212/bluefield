#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <curl/curl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define MAX_CLIENTS 20
#define TRUE 1
#define FALSE 0
#define TRYS_NUM 2


void get_ip_location(char * ip);
int Search_in_File(char * fname, char *str);
void * ftpSession(void * args);
void convert_to_str(char *string_ip, uint32_t int_ip);

struct cli_data {

    int sock_id;
    struct sockaddr_in cli_addr;

} cli_data;


int main(void)

{

	int sockfd, connfd, len, num_of_cli = 0;

    struct sockaddr_in servaddr, cli;

    pthread_t thread_id[MAX_CLIENTS];

    struct cli_data client_data;


    // socket create and verification

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd == -1) {

        printf("socket creation failed...\n");

        exit(0);

    }

    else

        printf("Socket successfully created..\n");

   
    int enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        error("setsockopt(SO_REUSEADDR) failed");
   
    
    bzero(&servaddr, sizeof(servaddr));


    // assign IP, PORT

    servaddr.sin_family = AF_INET;

    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    servaddr.sin_port = htons(PORT);


    // Binding newly created socket to given IP and verification

    if ((bind(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr))) != 0) {

        printf("socket bind failed...\n");

        exit(0);

    }

    else

        printf("Socket successfully binded..\n");


    // Now server is ready to listen and verification

    if ((listen(sockfd, MAX_CLIENTS)) != 0) {

        printf("Listen failed...\n");

        exit(0);

    }

    else

        printf("Server listening..\n");

    len = sizeof(cli);


    
    
    // Accept the data packet from client and verification

    //while(0)
    {


      connfd = accept(sockfd, (struct sockaddr *) &cli, &len);

      if (connfd < 0) {


          printf("server acccept failed...\n");

          exit(0);

      }

      else {


          printf("server acccept the client...\n");

          client_data.sock_id = connfd;
          
          client_data.cli_addr = cli;
        
          pthread_create(&thread_id[num_of_cli], NULL, ftpSession, &client_data);

          num_of_cli ++;

      }
      
      pthread_join(thread_id[0],0);



    }


    return 0;

}



void * ftpSession(void * args){

    char * str2;

    struct cli_data * cli;

    cli = args;

    char str[1024] = {0}, cli_input[1024] ={0}, check[1024]={0}, user_name[1024]={0}, pass[1024]={0};

    int size, flag = FALSE, i;

    char * str_ip;int j;

    /*str2 = inet_ntoa(cli->cli_addr.sin_addr);
    
    printf("%s", str2);
    
    get_ip_location( str2 );

    */
    
    strcpy(str, "NOWWWWWWWW - Connected to Mellanox.\n\r");

    size = strlen(str);

    send(cli->sock_id, str, size, 0);
    
    strcpy(str,"220 CentOS Linux FTP service (version 7)\n\r");

    size = strlen(str);
    
    send(cli->sock_id, str, size, 0);

    for( i = 0 ; i < TRYS_NUM ; i ++ ){


        strcpy(str, "USER : ");

        size = strlen(str);
        
        printf("%s",str);

        send(cli->sock_id, str, size, 0);
      
        recv(cli -> sock_id, user_name, 1024, 0);
      
        user_name[5]='\0';
      
        for(j =0; j< 10 ; j++){
            
            printf("%d ",*(user_name + j));
            
        }
        bzero(str,1024);
      
        sprintf(str, "331 Password requird for %s\n\rPassword : \r", user_name);
      
        size = strlen(str);
        
        printf("%s",str);

        send(cli->sock_id, str, size, 0);
      
        recv(cli -> sock_id, pass, 1024, 0);
      
        
        if( strcmp(user_name,"user") == 0 && strcmp(pass,"pass") == 0 ) {
            
            printf("right\n\r");
            
        } else {
            
            printf("wrong\n\r");
            
        }
    }

}



int Search_in_File(char * fname, char *str) {


	FILE * fp;

	int line_num = 1;

	int find_result = 0;

	char temp[512];


	if( (fp = fopen(fname,"r")) != NULL ) {

		return(-1);

	}


	while(fgets(temp, 512, fp) != NULL) {


  	if((strstr(temp, str)) != NULL) {


  		printf("A match found on line: %d\n\r", line_num);

			printf("\n%s\n", temp);


      if(fp) {

    		fclose(fp);

    	}


      return TRUE;

		}

	}


	if(find_result == 0) {

		printf("\nSorry, couldn't find a match.\n\r");

	}


	//Close the file if still open.

	if(fp) {

		fclose(fp);

	}

  return FALSE;

}


void get_ip_location(char * ip){

        printf("%s",ip);

    /*FILE * f;
    char str[1024] = {0};
    CURL *curl;
    CURLcode res;
    //curl_global_init(CURL_GLOBAL_DEFAULT);
    printf("1");
    //curl = curl_easy_init();
    printf("2");
    if(curl) {
        
        sprintf(str,"https://ipapi.co/%s/json/", ip);
        printf("%s",str);
        //curl_easy_setopt(curl, CURLOPT_URL, str);
        //printf("4");
        //res = curl_easy_perform(curl);*/
        /* Check for errors */ 
        /*if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
     */
        /* always cleanup */ 
        /*printf("5");
        curl_easy_cleanup(curl);
    }
    printf("67");
    curl_global_cleanup();*/
    
}


